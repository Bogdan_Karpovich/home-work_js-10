  // 1
  const learnMoreLink = document.createElement('a');

  learnMoreLink.textContent = 'Learn More';
  learnMoreLink.setAttribute('href', '#');

  const footer = document.querySelector('footer');

  footer.appendChild(learnMoreLink);

  // 2
   const selectRating = document.createElement('select');
   selectRating.id = 'rating';
 
   function createOption(value, text) {
     const option = document.createElement('option');
     option.value = value;
     option.textContent = text;
     return option;
   }
 
   const option4 = createOption('4', '4 Stars');
   const option3 = createOption('3', '3 Stars');
   const option2 = createOption('2', '2 Stars');
   const option1 = createOption('1', '1 Star');
 
   selectRating.appendChild(option4);
   selectRating.appendChild(option3);
   selectRating.appendChild(option2);
   selectRating.appendChild(option1);
 
   const main = document.querySelector('main');
   const featuresSection = document.querySelector('.features');
   main.insertBefore(selectRating, featuresSection);